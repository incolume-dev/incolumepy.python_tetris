# !/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = "@britodfbr"  # pragma: no cover

from incolumepy.fractal.db.nihil import inanis as dbinanis
from incolumepy.fractal.forms.nihil import inanis as finanis
from incolumepy.fractal.exceptions import NiHilError
from incolumepy.python_tetris.forms.square import square
from incolumepy.python_tetris.forms.trapeze import trapeze


def truncus():
    result = ""
    result += f"{dbinanis()}\n"
    result += f"{finanis()}\n"
    result += f"{square(a=1, b=2)}\n"
    result += f"{trapeze(1,2,3,4, x=1, y=2)}\n"
    return result


def truncus1(error=False):
    if error:
        raise NiHilError("Coisa Nenhuma!!")
    return "Ok."


if __name__ == '__main__':
    truncus()
